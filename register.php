<?php
include "session.php";
include "create.php";
include_once "Models/Category.php";
include "header.php";

if(isset($_SESSION['id'])){
  header('location: results.php');
  exit(0);
}

 if (count($errors) > 0): ?>
  <div class="alert alert-danger">
    <?php foreach ($errors as $error): ?>
    <li>
      <?php echo $error; ?>
    </li>
    <?php endforeach;?>
  </div>
<?php endif;

$category = new Models\Category;
?>


<!-- REGISTER FORM -->
<div class="row justify-content-center mt-5">
  <div class="col-lg-5 col-sm-12">
    <h2 class="text-center pb-3">Register</h2>

    <form action="" method="POST">
      <div class="form-group">
        <label class="font-weight-bold" for="name">Name</label>
        <input type="text" class="form-control" name="name" id="name">  
      </div>

      <div class="form-group">
        <label class="font-weight-bold" for="email">Email</label>
        <input type="email" class="form-control" name="email" id="email">
      </div>

      <div class="form-group">
        <label class="font-weight-bold" for="password">Password</label>
        <input type="password" class="form-control" name="password" id="password">
      </div>

      <div class="form-group">
        <label class="font-weight-bold" for="confirm">Confirm Password</label>
        <input type="password" class="form-control" name="confirm" id="confirm">
      </div>

      <div class="select-boxes">
        <label>Select what you work</label>
        <select class="form-control" name="category">
          <?php  $category->categoryTree(); ?>
        </select>
      </div>
      <input type="submit" value="Register" name="signup-btn" class="btn btn-primary btn-block mt-4 mb-5">
    </form>

    <p class="pb-5">Already have an account? <a href="login.php">Log in</a></p>
  </div>
</div>


</body>
</html>