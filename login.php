<?php
include "session.php";
include "create.php";
include "header.php";

if(isset($_SESSION['id'])){
  header('location: results.php');
  exit(0);
}

if (count($errors) > 0) : ?>
  <div class="alert alert-danger">
    <?php foreach ($errors as $error) : ?>
      <li>
        <?php echo $error; ?>
      </li>
    <?php endforeach; ?>
  </div>
<?php endif;
?>


<div class="row justify-content-center mt-5">
  <div class="col-lg-5 col-sm-12">
    <h2 class="pb-5">Login</h2>
    <form action="login.php" method="POST">
      <div class="form-group">
        <label class="font-weight-bold" for="email">Email</label>
        <input type="email" class="form-control" name="email" id="name" value=" <?php echo $email; ?>">
      </div>
      <div class="form-group">
        <label class="font-weight-bold" for="password">Password</label>
        <input type="password" class="form-control" name="password" id="password">
      </div>

      <button type="submit" name="login-btn" class="btn btn-primary btn-block mt-4 mb-5">Submit</button>
    </form>
    <p>Don't have an account yet? <a href="register.php">Sign up</a></p>


    </body>

    </html>