<?php
include_once "session.php";
include_once "Models/User.php";

$username = "";
$email = "";
$errors = [];
$user = new Models\User();

if (isset($_POST['signup-btn'])) {
    if (empty($_POST['name'])) {
        $errors['name'] = 'Username required';
    }
    if (empty($_POST['email'])) {
        $errors['email'] = 'Email required';
    }
    if (empty($_POST['password'])) {
        $errors['password'] = 'Password required';
    }
    if (isset($_POST['password']) && $_POST['password'] !== $_POST['confirm']) {
        $errors['confirm'] = 'The two passwords do not match';
    }

    $username = $_POST['name'];
    $email = $_POST['email'];
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $category = $_POST['category'];


    // Check if email already exists
  
    if ($user->emailExists($email)) {
        $errors['email'] = "Email already exists";
    }

    if (count($errors) === 0) {
       $user = new Models\User;
       $user->register($username, $email, $password, $category);
    }
}

// LOGIN
if (isset($_POST['login-btn'])) {
    if (empty($_POST['email'])) {
        $errors['email'] = 'Email required';
    }
    if (empty($_POST['password'])) {
        $errors['password'] = 'Password required';
    }
    $email = $_POST['email'];
    $password = $_POST['password'];


    if (count($errors) === 0) {
      
        $user->login($email, $password);
    }
}
