<?php
require "session.php";
include "header.php";

?>

<div class="container-fluid text-center mt-5 pt-5">
    <div class="row justify-content-center">
        <div class="col-lg-5 col-sm-10">
            <h1 class="pb-5">Welcome to our page</h1>
            <h4>To continue you have to register first!</h4>
            <a class="btn btn-success btn-block mb-5" href="register.php">Register</a>
            <h4>If you alredy have an account just login</h4>
            <a class="btn btn-primary btn-block" href="login.php">Login</a>
        </div>
    </div>
</div>