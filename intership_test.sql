-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2021 at 10:56 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intership_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `password` varchar(256) NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `category`) VALUES
(71, 'Tara', 'tara@example.com', '$2y$10$lFfuH3R8ZGH.T2qM7LIINOc0Y', 9),
(72, 'ana', 'ana@example.com', '$2y$10$Qslgj29vRDEZnA7kAElHKe/40', 12),
(73, 'Naum', 'naum@example.com', '$2y$10$NcN6H709HJIg7hJQwfo0AOQvzBZayCPnyvL4CYVUZEPp4DPK7GGdq', 5),
(74, 'ivana', 'ivana@example.com', '$2y$10$cjdf7l3VnSktN1PP8coa3ukVR', 11),
(75, 'Ivana1234', 'ivana1234@example.com', '$2y$10$plHOGCqCUbZeAuBRICDxxuMMb', 16),
(76, 'Ana123', 'ana123@angular.com', '$2y$10$hA.5NgVZq/Zq1k.A9FmDauMrg', 1),
(77, 'Mila', 'mila@example.com', '$2y$10$ZCmhY7jjWv4KRdTA6zm5EeYOO', 8),
(78, 'Miki', 'miki@example.com', '$2y$10$geLHhDVwYyJ.ojahYNjYleoAN', 2),
(79, 'Aleksandar', 'alek@example.com', '$2y$10$2FVzgY28ksARtqjmkrr15.SMh', 12),
(80, 'Borche', 'borche@example.com', '$2y$10$lOOFqSoG4QXAs.wCw66uIOXuuuoVR3RkFDCLY7oP7WZjAHDYZBDfy', 9),
(81, 'Baci', 'baci@example.com', '$2y$10$1LJLCxmZ3viNR2iB5EEaSOT0U21CZu7YLT7..Ub8wz6HLEow8I.82', 1),
(82, 'viki', 'viki@example.com', '$2y$10$modlJCnHoNtAFS35C.hI4uMk2sHUIWYJCx50h2K6iZbqJoxw7LBH2', 10),
(83, 'Ema', 'ema@example.com', '$2y$10$T.euwRHr7d9gaYAuAyCDE.pdLw0vtVZN/JrYB.GFgPQN8/CrpcJZK', 2),
(84, 'tina', 'tina@tina.co', '$2y$10$dcLOS/TOOGmfK8azkHTpGueUfEDeTY9eJrbrySYyj8mU5c7AH6hR.', 4),
(85, 'Joce', 'joce@joce.com', '$2y$10$bKgHVHwXqSWEyfBcs/rVS.l4BNH8ALmccwnJR9CIQuDZkIiYe06f6', 15),
(86, 'Mila123', 'mila123@example.com', '$2y$10$j.l4V/AfGqpBIucdn7DKDetFh1jvkC963W8.5v25Ln9p1oIDH11OK', 5),
(87, 'Teo', 'teo@example.com', '$2y$10$iQfD9BDL3tiUHm3iUXW8weKdrNq5EwBNeaR/.zXG0XmgEDpYNXwXy', 9),
(88, 'teo1', 'teo1@example.com', '$2y$10$rufj0wYwVUfGTnyCuX6cEuVTMdrngHa20y79ZNEww75lYApNDBBdS', 2),
(89, 'Joro', 'joro@example.com', '$2y$10$qRUi8DaoSH3jtuWs9gAGaOuFJ7PqD2ATgoxCyhjyq/yaPnus714wK', 2),
(90, 'Aleksandra Nikolovska', 'aleksandra_nikolovska_13@hotmail', '$2y$10$4XBmin5VlwKgaSmRpB//e.JgV29gLzOQgyHwg3XtvXHZXcQj7hxUe', 12);

-- --------------------------------------------------------

--
-- Table structure for table `users_type`
--

CREATE TABLE `users_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_type`
--

INSERT INTO `users_type` (`id`, `name`, `parent_id`) VALUES
(1, 'Front End Developer', 0),
(2, 'Back End Developer', 0),
(3, 'Angular', 1),
(4, 'React', 1),
(5, 'Angular JS', 3),
(6, 'Angular 2', 3),
(7, 'React Native', 4),
(8, 'Vue', 1),
(9, 'PHP', 2),
(10, 'Symfony', 9),
(11, 'Silex', 10),
(12, 'Laravel', 9),
(13, 'Lumen', 12),
(14, 'NodeJS', 2),
(15, 'Express', 14),
(16, 'NestJS', 14);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_type`
--
ALTER TABLE `users_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `users_type`
--
ALTER TABLE `users_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
