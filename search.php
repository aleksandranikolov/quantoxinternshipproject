<?php
include_once "session.php";
include_once "Models/Search.php";
include "header.php";

$search_query = '';
if(isset($_GET['search'])) {
    $search_query = $_GET['search'];
}

$search_object = new Models\Search();

$search  = $search_object->searchUsers($search_query);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <?php if (count($search) > 0) { ?>
                <div class="px-4">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Job</th>
                            </tr>
                        </thead>
                        <?php foreach ($search as $row) { ?>
                            <tr>
                                <td><?php echo $row["name"] ?></td>
                                <td><?php echo $row["email"] ?></td>
                                <td><?php echo $row["job"] ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                <?php } else { ?>
                    <h5>No Search results</h5>
                <?php } ?>
                </div>
        </div>
    </div>
</div>