<?php
Namespace Models;
require_once __DIR__ . "/BaseModel.php";



class Search extends BaseModel{

    public function searchUsers($search_query) {
        $query = '
        SELECT users.name, users.email, users_type.name AS job FROM `users` 
        JOIN users_type on users.category = users_type.id 
        WHERE users.name LIKE :search_query OR users.email LIKE :search_query OR users_type.name LIKE :search_query
        ';

        $search_query = trim($search_query);
        $search_query = "%$search_query%";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam("search_query", $search_query);
        $result = $stmt->execute();
        $search = $stmt->fetchAll();
        return $search;
    }

}