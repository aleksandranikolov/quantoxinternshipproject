<?php

namespace Models;
require_once __DIR__. "/BaseModel.php";

class User extends BaseModel {
    public function login($email, $password) {
        $password_hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $query = "SELECT * FROM users WHERE email=:email LIMIT 1";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        $user = $stmt->fetch();

        if ($user && password_verify($password, $user['password'])) { // if password matches
            $_SESSION['id'] = $user['id'];
            $_SESSION['name'] = $user['name'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['message'] = 'You are logged in!';
            $_SESSION['type'] = 'alert-success';
            header('location: results.php');
            exit(0);
        } else {
            $_SESSION['message'] = "Wrong username / password";
            $_SESSION['type'] = "alert-danger";
        }
    }

    public function register($name, $email, $password, $category) {
        $query = "INSERT INTO users SET name=:name, email=:email, category=:category, password=:password";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':category', $category);
        $stmt->bindParam(':password', $password);
        $result = $stmt->execute();


       
        if($result) {
            $user_id = $this->pdo->lastInsertId();

            $_SESSION['id'] = $user_id;
            $_SESSION['name'] = $name;
            $_SESSION['email'] = $email;
            $_SESSION['message'] = 'You are logged in!';
            $_SESSION['type'] = 'alert-success';
            header('location: results.php');
            exit(0);
        } else {
            $_SESSION['error_msg'] = "Database error: Could not register user";
        }
    }

    public function emailExists($email){
        $sql = "SELECT * FROM users WHERE email=:email LIMIT 1";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':email', $email);
        $result = $stmt->execute();

        return $stmt->rowCount() > 0;
    } 
}