<?php

namespace Models;

require_once __DIR__ . "/../session.php";

class BaseModel
{
    public $pdo = null;
    public $servername = "localhost";
    public $username = "root";
    public $password = "";
    public $dbname = "intership_test";

    public function __construct()
    {
        $this->pdo = new \PDO('mysql:host=127.0.0.1;dbname=' . $this->dbname, $this->username, $this->password);
    }
}
