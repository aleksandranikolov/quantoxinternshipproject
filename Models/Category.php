<?php

namespace Models;

class Category extends BaseModel
{
    public function categoryTree($parent_id = 0, $sub_mark = '')
    {
        $query = $this->pdo->query("SELECT * FROM users_type WHERE parent_id = $parent_id ORDER BY name ASC");

        if ($query->rowCount() > 0) {
            while ($row = $query->fetch()) {
                echo '<option value="' . $row['id'] . '">' . $sub_mark . $row['name'] . '</option>';
                $this->categoryTree($row['id'], $sub_mark . '&nbsp; &nbsp;');
            }
        }
    }
}
