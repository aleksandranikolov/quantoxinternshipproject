<?php
Namespace Models;
require_once __DIR__ . "/BaseModel.php";



class Result extends BaseModel{

    public function results() { 
        $query = "SELECT COUNT(category) as categories_count, category, users_type.name FROM `users` JOIN users_type on users.category = users_type.id GROUP BY category ORDER BY `category` ASC";
        $stmt = $this->pdo->prepare($query);
        $result = $stmt->execute();
        $categories = $stmt->fetchAll();
        return $categories;

    }
}