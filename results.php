<?php
include "session.php";
include_once "Models/Result.php";
include "header.php";
include 'create.php';

if(!isset($_SESSION['id'])) {
    header('location: index.php');
    return;
}

$result_object = new Models\Result();
$categories = $result_object->results();
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 col-sm-12 border-right border-secondary mt-5">
            <p>Registered Users type list</p>
            <ul>
            <?php foreach($categories as $category){ ?>
                <li> <?php echo $category["name"] . "(" . $category["categories_count"] . ")" ?></li>
            <?php } ?>
            </ul>
        </div>

        <div class="col-md-8 col-sm-12 mt-5 pt-5">
            <?php if (isset($_SESSION['message'])) : ?>
                <div class="alert <?php echo $_SESSION['type'] ?>">
                    <?php
                    echo $_SESSION['message'];
                    unset($_SESSION['message']);
                    unset($_SESSION['type']);
                    ?>
                </div>
            <?php endif; ?>

            <h4>Welcome, <?php echo $_SESSION['name']; ?></h4>
            <h4>Your email is: <?php echo $_SESSION['email']; ?> </h4>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4 home-wrapper">

                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>